# Deep Learning Illustrated: LeNet-5 in Keras

This is my working copy for the example from

> Jon Krohn, et al. Deep Learning Illustrated. 1st ed., Addison-Wesley Professional, 2019, https://www.deeplearningillustrated.com.

## Setup

```
$ conda create --name lenet5 -f environment.yml
```

## Use

```
$ conda activate lenet5
$ jupyter notebook
```

and visit http://localhost:8888/notebooks/lenet5.ipynb.



